import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListDetailView extends StatelessWidget {
  ListDetailView({Key? key}) : super(key: key);
  final argument = Get.arguments == null ? {} : Get.arguments as Map;

  final count = 0.obs;
  final st = "G".obs;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('詳情頁 前一頁參數 ${argument["id"] ?? ""}'),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () {
              print('按下選單');
              Get.back(result: {"something": "帶參數並且pop回前一頁"});
            }),
      ),
      body: Center(
          child: Column(
        children: [
          Obx(
            () => Text("count1 ->" + count.toString()),
          ),
          Obx(
            () => Text(st.toString()),
          ),
          ElevatedButton(
            onPressed: () {
              count.value++;
              st.value += 'G';
            },
            child: Text('add'),
          ),
        ],
      )),
    );
  }
}
