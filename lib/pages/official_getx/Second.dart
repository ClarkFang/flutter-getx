import 'package:flutter/material.dart';
import 'package:get/get.dart';
import './SecondeController.dart';

//GetView<> 要事先在GetPage Get.put 綁定
//否則controller OnInit()會跑不進去
class Second extends GetView<SecondeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('second Route'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Obx(
              () {
                print("count1 rebuild");
                return Text('count1:${controller.count1}');
              },
            ),
            Obx(
              () {
                print("count2 rebuild");
                return Text('count2:${controller.count2}');
              },
            ),
            Obx(() {
              print("sum rebuild");
              return Text('sum:${controller.sum}');
            }),
            Obx(
              () => Text('Name: ${controller.user.value.name}'),
            ),
            Obx(
              () => Text('Age: ${controller.user.value.age}'),
            ),
            ElevatedButton(
              child: Text("Go to last page"),
              onPressed: () {
                Get.toNamed('/third', arguments: 'arguments of second');
              },
            ),
            ElevatedButton(
              child: Text("Back page and open snackbar"),
              onPressed: () {
                Get.back();
                Get.snackbar(
                  'User 123',
                  'Successfully created',
                );
              },
            ),
            ElevatedButton(
              child: Text("Increment count1"),
              onPressed: () {
                controller.increment();
              },
            ),
            ElevatedButton(
              child: Text("Increment count2"),
              onPressed: () {
                controller.increment2();
              },
            ),
            ElevatedButton(
              child: Text("Update name"),
              onPressed: () {
                controller.updateUser();
              },
            ),
            ElevatedButton(
              child: Text("Dispose worker"),
              onPressed: () {
                controller.disposeWorker();
              },
            ),
          ],
        ),
      ),
    );
  }
}
