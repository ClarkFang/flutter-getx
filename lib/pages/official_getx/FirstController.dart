import 'package:get/get.dart';

class FirstController extends GetxController {
  int count = 0;

  @override
  onInit() {
    print("");
  }

  void increment() {
    count++;
    // use update method to update all count variables
    update();
  }
}
