import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practice_getx/pages/official_getx/FirstController.dart';

class First extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // leading: IconButton(
        //   icon: Icon(Icons.add),
        //   onPressed: () {
        //     Get.snackbar("Hi", "I'm modern snackbar");
        //   },
        // ),
        title: Text("title".trArgs(['John', '第二個變數', 'hzn'])),
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Get.snackbar("Hi", "I'm modern snackbar");
            },
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GetBuilder<FirstController>(
                init: FirstController(),
                // You can initialize your controller here the first time. Don't use init in your other GetBuilders of same controller
                builder: (_) => Text(
                      'clicks: ${_.count}',
                    )),
            ElevatedButton(
              child: Text('Next Route'),
              onPressed: () {
                Get.toNamed('/second');
              },
            ),
            ElevatedButton(
              child: Text('Change locale to English'),
              onPressed: () {
                Get.updateLocale(Locale('en', 'UK'));
              },
            ),
            ElevatedButton(
              child: Text('切換繁體中文'),
              onPressed: () {
                Get.updateLocale(Locale('zh', 'TW'));
              },
            ),
            ElevatedButton(
              child: Text('切换简体中文'),
              onPressed: () {
                Get.updateLocale(Locale('zh', 'CN'));
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            //Get.find<FirstController>().increment();
            final FirstController ctrl = Get.find();
            ctrl.increment();
          }),
    );
  }
}
