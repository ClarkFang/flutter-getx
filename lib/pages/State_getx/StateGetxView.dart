import 'package:flutter/material.dart';
import 'package:get/get.dart';
import './StateGetxViewController.dart';

/*
 * GetBuilder與Getx差異在
 * Getx是呼叫controller的方法後會直接更新.
 * GetBuilder需要再呼叫controller.update() 才會更新
*/

class StateGetxView extends StatelessWidget {
  StateGetxView({Key? key}) : super(key: key);

  final controller = StateGetxViewController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Getx with controller"),
      ),
      body: Center(
        child: Column(
          children: [
            GetX<StateGetxViewController>(
              init: controller,
              initState: (_) {},
              builder: (_) {
                print("GetX - 1");
                return Text('count value 1 -> ${controller.count}');
              },
            ),
            GetX<StateGetxViewController>(
              init: controller,
              initState: (_) {
                //print('initState: $_');
              },
              builder: (_) {
                print("GetX - 1");
                return Text('count value 2-> ${controller.count2}');
              },
            ),
            Divider(),
            ElevatedButton(
              onPressed: () {
                controller.add();
              },
              child: Text('count1 ++'),
            ),
            ElevatedButton(
              onPressed: () {
                controller.add2();
              },
              child: Text('count2'),
            ),
          ],
        ),
      ),
    );
  }
}
