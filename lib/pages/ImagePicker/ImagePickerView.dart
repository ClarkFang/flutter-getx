import 'dart:io';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:practice_getx/pages/ImagePicker/ImagePickerViewController.dart';
import 'package:practice_getx/pages/ImagePickerDetail/ImagePickerDetail.dart';

class ImagePickerView extends GetView<ImagePickerViewController> {
  ImagePickerView({Key? key}) : super(key: key);
  //final controller = ImagePickerViewController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Image Picker")),
      body: Column(
        children: [
          Obx(
            () => Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: imageSelectWidgetList(context),
              ),
            ),
          ),
          ElevatedButton(
              onPressed: () {
                controller.everCountShow();
              },
              child: Text('ever')),
          Obx(() => Text('debounceCount: ${controller.debounceCount}')),
          ElevatedButton(
              onPressed: () {
                controller.debounceCountShow();
              },
              child: Text('debounce 2sec')),
          Obx(() => Text('intervalCount: ${controller.intervalCount}')),
          ElevatedButton(
              onPressed: () {
                controller.intervalCountShow();
              },
              child: Text('interval 2sec')),
          ElevatedButton(
              onPressed: () {
                Get.to(ImagePickerDetail());
              },
              child: Text('Next page')),
        ],
      ),
    );
  }

  List<Widget> imageSelectWidgetList(BuildContext context) {
    return controller.filePathList.map<Widget>((filePath) {
      final index = controller.filePathList.indexOf(filePath);
      return GestureDetector(
        onTap: () {
          showModalBottomSheet(
            context: context,
            builder: (context) {
              return Padding(
                padding: const EdgeInsets.all(12.0),
                child: Form(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      ListTile(
                        title: Center(
                          child: TextButton.icon(
                            onPressed: null,
                            icon: Icon(Icons.image),
                            label: Text("相簿"),
                          ),
                        ),
                        onTap: () {
                          print("點相簿");
                          //這裡的back是把BottomSheet做點選後的取消,不是回前一頁
                          Get.back();
                          controller.getImage(ImageSource.gallery, index);
                        },
                      ),
                      Divider(),
                      ListTile(
                        title: Center(
                          child: TextButton.icon(
                            onPressed: null,
                            icon: Icon(Icons.camera_alt),
                            label: Text("相機"),
                          ),
                        ),
                        onTap: () {
                          print("點相機");
                          //這裡的back是把BottomSheet做點選後的取消,不是回前一頁
                          Get.back();
                          controller.getImage(ImageSource.camera, index);
                        },
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        },
        child: Container(
          decoration: BoxDecoration(
              color: Colors.black45,
              border: Border.all(),
              borderRadius: BorderRadius.all(Radius.circular(5.0))),
          height: 100,
          width: 100,
          child:
              filePath == "" ? Icon(Icons.image) : Image.file(File(filePath)),
        ),
      );
    }).toList();
  }
}
