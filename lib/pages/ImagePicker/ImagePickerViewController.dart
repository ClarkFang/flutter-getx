import 'package:image_picker/image_picker.dart';
import 'package:get/get.dart';

class ImagePickerViewController extends GetxController {
  final _filePathList = ["", "", ""].obs;
  set filePathList(value) => this._filePathList.add(value);
  get filePathList => this._filePathList;

  final _everCount = 0.obs;
  set everCount(value) => this._everCount.value = value;
  get everCount => this._everCount.value;

  final _debounceCount = 0.obs;
  set debounceCount(value) => this._debounceCount.value = value;
  get debounceCount => this._debounceCount.value;

  final _intervalCount = 0.obs;
  set intervalCount(value) => this._intervalCount.value = value;
  get intervalCount => this._intervalCount.value;

  @override
  onInit() {
    super.onInit();
    ever(
        _everCount,
        (_) => Get.snackbar("每次按都會跑進ever 值:$_", "ever",
            duration: Duration(seconds: 2)));

    /// Anti DDos - Called every time the user stops typing for 1 second, for example.
    debounce(_debounceCount, (_) {
      Get.snackbar(
        "debounce 最後取到的值:$_",
        "收集一段時間內的改變,當改變停止2秒後,取最後一次的值做事",
        duration: Duration(seconds: 4),
      );
    }, time: Duration(seconds: 2));

    /// Ignore all changes within 1 second.
    interval(_intervalCount, (_) {
      Get.snackbar(
        "interval 第一次取到的值:$_",
        "第一次按就取值,但後面2秒內怎麼變都不理 不需要等待停下",
        duration: Duration(seconds: 4),
      );
    }, time: Duration(seconds: 2));
  }

  everCountShow() => _everCount.value++;
  debounceCountShow() => _debounceCount.value++;
  intervalCountShow() => _intervalCount.value++;

  Future getImage(ImageSource source, int didSelectIndex) async {
    final _picker = ImagePicker();
    final pickedFile = await _picker.getImage(source: source);

    if (pickedFile != null) {
      filePathList[didSelectIndex] = pickedFile.path;
    } else {
      //取消
      print("沒有選擇照片");
    }
  }
}
