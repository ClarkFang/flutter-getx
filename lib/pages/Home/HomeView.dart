import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practice_getx/env.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("首页 ${env!.flavor.toString()}"),
      ),
      body: ListView(
        children: [
          // 路由&导航
          ListTile(
            title: Text("Route home > list"),
            subtitle: Text('Get.toNamed("/home/list")'),
            onTap: () => Get.toNamed("/home/list"),
          ),
          ListTile(
            title: Text("Route home > list > detail"),
            subtitle: Text('Get.toNamed("/home/list/ListDetail")'),
            onTap: () => Get.toNamed("/home/list/ListDetail"),
          ),
          ListTile(
            title: Text("Route home > StateGetx"),
            subtitle: Text('Get.toNamed("/StateGetx")'),
            onTap: () => Get.toNamed("/home/StateGetx"),
          ),
          ListTile(
            title: Text("Route home > StateGetBuilder"),
            subtitle: Text('Get.toNamed("/StateGetBuilder")'),
            onTap: () => Get.toNamed("/home/StateGetBuilder"),
          ),
          ListTile(
            title: Text("官方範例1:GetBuilder Get.find, 多語系, counter, snackBar"),
            subtitle: Text('Get.toNamed("/home/OffcialGetxFirst")'),
            onTap: () => Get.toNamed("/home/OffcialGetxFirst"),
          ),
          ListTile(
            title: Text("官方範例2:GetView, 各型別的監聽綁定"),
            subtitle: Text('Get.toNamed("/home/OffcialGetxSecond")'),
            onTap: () => Get.toNamed("/home/OffcialGetxSecond"),
          ),
          ListTile(
            title: Text("Image picker"),
            subtitle: Text('Image picker'),
            onTap: () => Get.toNamed("/home/ImagePicker"),
          ),
        ],
      ),
    );
  }
}

class Todo {
  //String id;
  String? description = "";
  bool isCompleted;
  Todo({this.description})
      : //this.id = Uuid().v1(),
        this.isCompleted = false;
}
