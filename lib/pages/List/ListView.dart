import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListView extends StatelessWidget {
  const ListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('列表頁'),
      ),
      body: ElevatedButton(
        child: Text('詳情頁'),
        style: ElevatedButton.styleFrom(
          primary: Theme.of(context).accentColor,
        ),
        onPressed: () async {
          var popResponse = await Get.toNamed("/home/list/ListDetail",
              arguments: {"id": 666});
          Get.snackbar(
            "popResponse",
            "popResponse ->" + popResponse["something"].toString(),
          );
        },
      ),
    );
  }
}
