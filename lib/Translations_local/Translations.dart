import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/internacionalization.dart';
import 'package:get/get.dart';
import 'en_US.dart';
import 'zh_TW.dart';
import 'zh_CN.dart';

class MyTranslations extends Translations {
  //static Locale? get locale => Get.deviceLocale;
  //static final fallbackLocale = Locale('en', 'US');

  @override
  Map<String, Map<String, String>> get keys => {
        'en_UK': en_US,
        'zh_TW': zh_TW,
        'zh_CN': zh_CN,
      };
}
