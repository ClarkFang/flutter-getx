import 'env.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:practice_getx/routes/app_pages.dart';
import 'package:practice_getx/shared/logger/logger_utils.dart';
import 'Translations_local/Translations.dart';

void main() {
  BuildEnvironment.init(flavor: BuildFlavor.development, baseUrl: 'https://');
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: AppPages.initPage,
      getPages: AppPages.routes,
      defaultTransition: Transition.rightToLeft,
      translations: MyTranslations(),
      enableLog: true,
      logWriterCallback: Logger.write,
    );
  }
}
