import 'package:meta/meta.dart';

enum BuildFlavor { production, staging, development }

BuildEnvironment? get env => _env;
BuildEnvironment? _env;

class BuildEnvironment {
  /// The backend server.
  final String? baseUrl;
  BuildFlavor flavor = BuildFlavor.development;

  BuildEnvironment._init({required this.flavor, this.baseUrl});

  /// Sets up the top-level [env] getter on the first call only.
  static void init({@required flavor, @required baseUrl}) =>
      _env ??= BuildEnvironment._init(flavor: flavor, baseUrl: baseUrl);
}
