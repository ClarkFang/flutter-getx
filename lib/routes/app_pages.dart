import 'package:get/get.dart';
import 'package:practice_getx/pages/Home/HomeView.dart';
import 'package:practice_getx/pages/ImagePicker/ImagePickerView.dart';
import 'package:practice_getx/pages/ImagePicker/ImagePickerViewController.dart';
import 'package:practice_getx/pages/ImagePickerDetail/ImagePickerDetail.dart';
import 'package:practice_getx/pages/List/ListView.dart';
import 'package:practice_getx/pages/ListDetail/ListDetailView.dart';
import 'package:practice_getx/pages/State_get_builder/StateGetBuilderView.dart';
import 'package:practice_getx/pages/State_getx/StateGetxView.dart';
import 'package:practice_getx/pages/official_getx/First.dart';
import 'package:practice_getx/pages/official_getx/SecondeController.dart';
import 'package:practice_getx/pages/official_getx/Second.dart';

part 'app_routes.dart';

class AppPages {
  static const initPage = AppRoutes.Home;

  static final routes = [
    GetPage(
      name: AppRoutes.Home,
      page: () => HomeView(),
      children: [
        GetPage(
          name: AppRoutes.List,
          page: () => ListView(),
          children: [
            GetPage(
              name: AppRoutes.Detail,
              page: () => ListDetailView(),
              transition: Transition.downToUp,
            ),
          ],
        ),
        GetPage(
          name: AppRoutes.StateGetx,
          page: () => StateGetxView(),
        ),
        GetPage(
          name: AppRoutes.StateGetBuilder,
          page: () => StateGetBuilderView(),
        ),
        GetPage(
          name: AppRoutes.OffcialGetxFirst,
          page: () => First(),
          //binding: SampleBind(),
        ),
        GetPage(
          name: AppRoutes.OffcialGetxSecond,
          page: () => Second(),
          binding: SampleBind(),
        ),
        GetPage(
          name: AppRoutes.ImagePicker,
          page: () => ImagePickerView(),
          binding: SampleBind(),
          children: [
            GetPage(
              name: AppRoutes.ImagePickerDetail,
              page: () => ImagePickerDetail(),
            ),
          ],
        )
      ],
    ),
  ];
}

class SampleBind extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SecondeController>(() => SecondeController());
    Get.lazyPut<ImagePickerViewController>(() => ImagePickerViewController());
  }
}
