part of 'app_pages.dart';

abstract class AppRoutes {
  static const Home = '/home';
  static const List = '/list';
  static const Detail = '/ListDetail';
  static const StateGetx = '/StateGetx';
  static const StateGetBuilder = '/StateGetBuilder';
  static const OffcialGetxFirst = '/OffcialGetxFirst';
  static const OffcialGetxSecond = '/OffcialGetxSecond';
  static const ImagePicker = '/ImagePicker';
  static const ImagePickerDetail = '/ImagePickerDetail';
}
