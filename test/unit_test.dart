import 'package:flutter_test/flutter_test.dart';

class Counter {
  int value = 0;
  increment() => value++;
  decrement() => value--;
}

void main() {
  Counter counter = Counter();
  group('Counter', () {
    setUp(() {
      counter = Counter();
    });
    test('value should start at 0', () {
      expect(counter.value, 0);
    });

    test('value should be incremented', () {
      counter.increment();
      expect(counter.value, 1);
    });

    test('value should be decremented', () {
      print(counter.value);
      counter.decrement();
      expect(counter.value, -1);
    });
  });
}
